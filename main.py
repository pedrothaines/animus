#!/usr/bin/env python2.7

from flask import Flask, jsonify, render_template, request, url_for
import pysmu
import webbrowser
import random
from time import sleep

""" 
Multiplexer Mapping

+-------------------------------+
| PIO 0 | PIO 1 |     SENSOR    |
+-------------------------------+
|   0   |   0   | Temperature   |
|   0   |   1   | Soil Humidity |
|   1   |   0   | Luminosity    |
|   1   |   1   | Gas           |
+-------------------------------+
"""


class Device:
    def __init__(self):
        self.session = pysmu.Session()

        # self.session.add_all()

        self.device = self.session.devices[0]
        self.channel_a = self.device.channels['A']
        self.channel_b = self.device.channels['B']
        #self.device.ctrl_transfer(0x40, 0x51, 0, 0, 0, 0, 100)

        #self.channel_a.mode = pysmu.Mode.SIMV
        #self.channel_b.mode = pysmu.Mode.SIMV
        #self.set_pio_mode(0, 'OUTPUT')
        #self.set_pio_mode(1, 'OUTPUT')
        #self.set_pio_mode(2, 'OUTPUT')
        #self.set_pio_mode(3, 'OUTPUT')

    def get_channelA_samples(self, n_samples):
        samples = []
        try:
            samples = self.channel_a.get_samples(n_samples)
        except:
            print("SYS ERROR: failed while trying to get {samples} sample(s) from channel A".format(samples=n_samples))

        return samples

    def get_channelB_samples(self, n_samples):
        samples = []
        try:
            samples = self.channel_b.get_samples(n_samples)
        except:
            print("SYS ERROR: failed while trying to get {samples} sample(s) from channel B".format(samples=n_samples))

        return samples

    def get_samples_from_both_channels(self, n_samples):
        try:
            samples = self.device.get_samples(n_samples)
        except:
            print("SYS ERROR: failed while trying to get {samples} sample(s) from both channels (A and B)".format(
                samples=n_samples))
        return samples

    def set_pio_mode(self, pio_port_number, pio_mode):
        if pio_mode == 'INPUT':
            try:
                self.device.ctrl_transfer(0xc0, 0x91, pio_port_number, 0, 0, 1, 100)
            except:
                print("SYS ERROR: failed while trying to set pin {pio} as INPUT".format(pio=pio_port_number))

        elif pio_mode == 'OUTPUT':
            try:
                self.device.ctrl_transfer(0x40, 0x50, pio_port_number, 0, 0, 0, 100)
            except:
                print("SYS ERROR: failed while trying to set pin {pio} as OUTPUT".format(pio=pio_port_number))

    def set_pio_state(self, pio_port_number, pio_state):
        if pio_state == 'HIGH':
            try:
                self.device.ctrl_transfer(0x40, 0x51, pio_port_number, 0, 0, 0, 100)
            except:
                print("SYS ERROR: failed while trying to set pin {pio} HIGH".format(pio=pio_port_number))

        elif pio_state == 'LOW':
            try:
                self.device.ctrl_transfer(0x40, 0x50, pio_port_number, 0, 0, 0, 100)
            except:
                print("SYS ERROR: failed while trying to set pin {pio} HIGH".format(pio=pio_port_number))


class SensorHandler(Device):
    def __init__(self, device):
        self.device = device

    def get_temperature(self):
        #self.device.set_pio_state(0, 0)
        #self.device.set_pio_state(1, 0)
        sleep(0.1)
        samples = self.device.get_channelB_samples(10000)

        measured_voltage_list = []
        for v in samples:
            measured_voltage_list.append(v[0])

        measured_voltage_average = sum(measured_voltage_list) / len(measured_voltage_list)

        return round(measured_voltage_average, 5)


m1k = Device()

app = Flask(__name__)

m1k.set_pio_state(0, "HIGH")
sleep(5)
m1k.set_pio_state(0, "LOW")

@app.route('/')
def index():
    return render_template('index.html')


@app.route("/m1k/sensors/temperature")
def temperature():
    data = {}

    voltage = SensorHandler(m1k).get_temperature()
    temp_celsius = round( (voltage * 100), 2)
    temp_fahrenheit = round( ((temp_celsius * (9/5)) + 32), 2)
    temp_kelvin = round( (temp_celsius + 273.15), 2)

    data = {
        "voltage": voltage,
        "temperature_celsius": temp_celsius,
        "temperature_fahrenheit": temp_fahrenheit,
        "temperature_kelvin": temp_kelvin
    }

    return jsonify(data)


@app.route("/m1k/sensors/soilmoisture")
def soil_moisture():
    data = {
        "voltage": round(random.uniform(0,5),2),
    }
    return jsonify(data)


@app.route("/m1k/sensors/gas")
def gas():
    data = {
        "voltage": round(random.uniform(0,5),2),
    }
    return jsonify(data)


@app.route("/m1k/sensors/flame")
def flame():
    data = {
        "voltage": round(random.uniform(0,5),2),
    }
    return jsonify(data)


@app.route("/m1k/sensors/all")
def all():
    voltage = SensorHandler(m1k).get_temperature()
    temp_celsius = round( (voltage * 100), 2)
    temp_fahrenheit = round( ((temp_celsius * (9/5)) + 32), 2)
    temp_kelvin = round( (temp_celsius + 273.15), 2)

    data = {
        "temperature_sensor": { "voltage": voltage,
                                "temperature_celsius": temp_celsius,
                                "temperature_fahrenheit": temp_fahrenheit,
                                "temperature_kelvin": temp_kelvin
                              },
        "soil_humidity_sensor": {"voltage": round(random.uniform(0,5),2), "humidity": 44},
        "gas_sensor": {"voltage": round(random.uniform(0,5),2), "gas_ppm": 44},
        "flame_sensor": {"voltage": round(random.uniform(0,5),2), "flame_status": False},
    }

    return jsonify(data)


webbrowser.open_new_tab('http://127.0.0.1:5000')
