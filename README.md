# ANIMUS

Analog Devices ADALM 1000 (M1K) controller

## Installation
1. Install the latest stable version of **VirtualBox** available at https://www.virtualbox.org/
2. Install the latest stable version of **Vagrant** available at https://www.vagrantup.com/
